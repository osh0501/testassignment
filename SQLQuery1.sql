﻿insert into AllSectors (Name, ParentId) values 
('Manufacturing', 0),
('Construction materials', 1),
('Electronics and Optics', 1),
('Food and Beverage', 1),
('Furniture', 1),
('Machinery', 1),
('Metalworking',1),
('Plastic and Rubber',1),
('Printing',1),
('Textile and Closing',1),
('Wood',1),

('Bakery & confectionery products',4),
('Beverages',4),
('Fish & fish products',4),
('Meat & meat products',4),
('Milk & dairy products',4),
('Other (Food)',4),
('Sweet & snack food',4),

('Bathroom/sauna',5),
('Bedroom',5),
('Children room''s',5),
('Kitchen', 5),
('Living room', 5),
('Office', 5),
('Other (Furniture)', 5),
('Outdoor', 5),
('Project furniture', 5),

('Machinery components',6),
('Machinery equipment/tools',6),
('Manufacture of machinery', 6),
('Maritime', 6),
('Metal structures', 6),
('Other (Machinery)', 6),
('Repair and maintenance service', 6),

('Aluminium and steel workboats', 31),
('Boat/Yacht building', 31),
('Ship repair and conversion',31),

('Construction of metal structures',7),
('Houses and buildings',7),
('Metal products',7),
('Metal works',7),

('CNC-machining',41),
('Forgings, Fasteners',41),
('Gas, Plasma, Laser cutting',41),
('MIG, TIG, Aluminum welding',41),

('Packaging',8),
('Plastic goods',8),
('Plastic processing technology',8),
('Plastic profiles',8),

('Blowing',48),
('Moulding',48),
('Plastics welding and processing',48),

('Advertising',9),
('Book/Periodicals printing',9),
('Labeling and packaging printing',9),

('Clothing',10),
('Textile',10),

('Other (Wood)',11),
('Wooden building materials',11),
('Wooden houses',11),

('Other', 0),

('Creative industries', 61),
('Energy technology', 61),
('Environment', 61),

('Service', 0),

('Business services', 65),
('Engineering', 65),
('Information Technology and Telecommunications', 65),
('Tourism', 65),
('Translation services', 65),
('Transport and Logistics', 65),

('Data processing, Web portals, E-marketing', 68),
('Programming, Consultancy', 68),
('Software, Hardware', 68),
('Telecommunications', 68),

('Air', 71),
('Rail', 71),
('Road', 71),
('Water', 71)