﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestAssignment.Models
{
    public class UserFormViewModel
    {
        [Required(ErrorMessage = "Please provide your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please choose sectors you are involved in")]
        [Display(Name = "Sectors")]
        public List<int> SelectedValues { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Please agree to terms")]
        [Display(Name = "Agree to terms")]
        public bool TermsIsAccepted { get; set; }
    }
}