﻿using System.Collections.Generic;

namespace TestAssignment.Models
{
    public class SectorsStructure
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public List<SectorsStructure> Sectors { get; set; }
    }
}