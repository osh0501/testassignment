﻿using System.Collections.Generic;
using System.Linq;
using TestAssignment.Database.Entities;
using TestAssignment.Models;

namespace TestAssignment.Database
{
    public class DbHelper
    {
        public static void RemoveDuplicatedRecords(string name, string username)
        {
            using (var db = new CustomDbContext())
            {
                var duplicatedRecords = db.DataFromUsers.Where(c => c.Name == name && c.Username == username).Select(c => c).ToList();
                if (duplicatedRecords.Count != 0)
                {
                    db.DataFromUsers.RemoveRange(duplicatedRecords);
                    db.SaveChanges();
                }
            }
        }

        public static List<SectorsStructure> GetData(int parentId)
        {
            var titles = new List<SectorsStructure>();
            using (var db = new CustomDbContext())
            {
                var allParentIds = db.AllSectors.Select(c => c.ParentId).Distinct().ToList();
                var allItemsPerParentId = db.AllSectors.Where(c => c.ParentId == parentId).Select(c => c).ToList();
                if (allItemsPerParentId.Count > 0)
                {
                    foreach (var item in allItemsPerParentId)
                    {
                        titles.Add(allParentIds.Contains(item.Id)
                            ? new SectorsStructure { Name = item.Name, Id = item.Id, Sectors = GetData(item.Id) }
                            : new SectorsStructure { Name = item.Name, Id = item.Id, Sectors = null });
                    }
                }
            }
            return titles;
        }

        public static void SaveToDb(UserFormViewModel model, string username)
        {
            using (var db = new CustomDbContext())
            {
                var userData = new DataFromUser();
                foreach (var item in model.SelectedValues)
                {
                    userData.Name = model.Name;
                    userData.TermsIsAccepted = model.TermsIsAccepted;
                    userData.SelectedValue = item;
                    userData.Username = username;
                    db.DataFromUsers.Add(userData);
                    db.SaveChanges();
                }
            }
        }

    }
}
