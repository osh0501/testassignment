﻿using System.Data.Entity;
using TestAssignment.Database.Entities;

namespace TestAssignment.Database
{
    public class CustomDbContext: DbContext
    {
        public CustomDbContext() : base("DefaultConnection")
        {
        }
        public DbSet<AllSector> AllSectors { get; set; }
        public DbSet<DataFromUser> DataFromUsers { get; set; }
    }
}