﻿namespace TestAssignment.Database.Entities
{
    public class DataFromUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool TermsIsAccepted { get; set; }
        public int SelectedValue { get; set; }
        public string Username { get; set; }
    }
}