﻿namespace TestAssignment.Database.Entities
{
    public class AllSector
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
    }
}