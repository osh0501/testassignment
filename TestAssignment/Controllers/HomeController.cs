﻿using System.Web.Mvc;
using TestAssignment.Database;
using TestAssignment.Models;

namespace TestAssignment.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            var allData = DbHelper.GetData(0);
            ViewBag.allData = allData;
            return View();
        }

        [HttpPost]
        public ActionResult Index(UserFormViewModel model)
        {
            var allData = DbHelper.GetData(0);
            ViewBag.allData = allData;
            var username = User.Identity.Name;

            DbHelper.RemoveDuplicatedRecords(model.Name, username);

            if (ModelState.IsValid)
            {
                DbHelper.SaveToDb(model, username);
                return View();
            }

            return View(model);
        }
    }
}